#!/usr/bin/env python3

"""
Script template. 

author: Simon Berlendis
date: 10/2021
"""

import os
import sys
import argparse
import numpy as np
import pandas as pd
import re

def main():
    """
    Main function
    
    Parameters:
    
    Returns:
    """ 
    pass

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='template.')
    ##  parser.add_argument('infile', help="Input file.")
    ## parser.add_argument("-o", "--output", dest='oufile', default='Result.txt', help='Output file (Default: Result.txt).')

    args = parser.parse_args()
    
    main()

